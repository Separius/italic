import torch
import torch.nn as nn


def localize(x: torch.Tensor, context_size: int = 16, query_size: int = 4):
    batch_size, num_channels, height, width = x.size()
    # x is B, C, H, W -> pad with (context_size - query_size) ->
    padding = (context_size - query_size) // 2
    x = nn.Unfold(context_size, padding=padding, stride=query_size)(x)
    x = x.view(batch_size, num_channels, context_size, context_size, height // query_size, width // query_size)
    # B, C, Context, Context, Height / Query, Width / Query
    x = x.permute(0, 4, 5, 1, 2, 3).contiguous()
    x = x.view(-1, x.size(3), x.size(4), x.size(5))  # B * Height/Query * Width/Query, C, Context, Context
    # TODO apply self attention here
    x = x[..., padding:, padding:][..., :-padding, :-padding]  # B * H/Q * W/Q, C, Q, Q
    x = x.view(batch_size, height // query_size, width // query_size, num_channels, query_size, query_size)
    x = x.permute(0, 3, 4, 1, 5, 2).contiguous()
    return x.view(batch_size, num_channels, height, width)
