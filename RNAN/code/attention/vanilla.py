import torch
import numpy as np
import torch.nn as nn
import torch.nn.functional as F


def init_default(m: nn.Module, func=nn.init.kaiming_normal_) -> nn.Module:
    if func:
        if hasattr(m, 'weight'): func(m.weight)
        if hasattr(m, 'bias') and hasattr(m.bias, 'data'): m.bias.data.fill_(0.)
    return m


def conv2d(ni: int, nf: int, ks: int = 3, stride: int = 1, padding: int = None, bias=False,
           init=nn.init.kaiming_normal_) -> nn.Conv2d:
    if padding is None: padding = ks // 2
    return init_default(nn.Conv2d(ni, nf, kernel_size=ks, stride=stride, padding=padding, bias=bias), init)


class PositionalEmbedding1d(nn.Module):
    def __init__(self, d_model, seq_len):
        super().__init__()
        pe = torch.zeros(seq_len, d_model).float()
        pe.require_grad = False
        position = torch.arange(0, seq_len).float().unsqueeze(1)
        div_term = (torch.arange(0, d_model, 2).float() * -(np.log(10000.0) / d_model)).exp()
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.permute(1, 0).unsqueeze(0)
        self.register_buffer('pe', pe)

    def forward(self, x):
        return self.pe


class PositionalEmbedding2d(nn.Module):
    def __init__(self, d_model, seq_len=64, learned=False):
        super().__init__()
        if not learned:
            assert d_model % 2 == 0
            embedding = PositionalEmbedding1d(d_model // 2, seq_len)
            self.register_buffer('embedding', torch.cat([
                embedding.pe.unsqueeze(3).expand(-1, -1, -1, seq_len),
                embedding.pe.unsqueeze(2).expand(-1, -1, seq_len, -1)
            ], dim=1))
        else:
            self.embedding = torch.nn.Parameter(torch.randn(1, d_model, seq_len, seq_len))

    def forward(self, x):
        return self.embedding


def gelu(x):
    return x * 0.5 * (1.0 + torch.erf(x / np.sqrt(2.0)))


def swish(x):
    return x * torch.sigmoid(x)


class PositionwiseFF(nn.Module):
    def __init__(self, d_model, d_inner, dropout, pre_lnorm=False):
        super().__init__()
        self.d_model = d_model
        self.d_inner = d_inner
        self.dropout = dropout

        self.CoreNet = nn.Sequential(
            nn.Conv1d(d_model, d_inner, 1), nn.ReLU(True),
            nn.Dropout(dropout),
            nn.Conv1d(d_inner, d_model, 1),
            nn.Dropout(dropout),
        )

        self.layer_norm = nn.LayerNorm(d_model)

        self.pre_lnorm = pre_lnorm

    def forward(self, inp):
        if self.pre_lnorm:
            ##### layer normalization + positionwise feed-forward
            core_out = self.CoreNet(self.layer_norm(inp))
            ##### residual connection
            output = core_out + inp
        else:
            ##### positionwise feed-forward
            core_out = self.CoreNet(inp)
            ##### residual connection + layer normalization
            output = self.layer_norm(inp + core_out)
        return output


class VanillaSelfAttention2d(nn.Module):
    def __init__(self, n_channels: int, use_max_pooling: bool = True, num_heads: int = 1):
        super().__init__()
        self.ch = n_channels
        self.query = conv2d(n_channels, n_channels // 8 * num_heads, 1)
        self.key = conv2d(n_channels, n_channels // 8 * num_heads, 1)
        self.value = conv2d(n_channels, n_channels // 2 * num_heads, 1)
        self.o = conv2d(n_channels // 2 * num_heads, n_channels, 1)
        self.gamma = nn.Parameter(torch.tensor([0.0]))
        self.max_pool = nn.MaxPool2d(2) if use_max_pooling else nn.Identity()
        self.num_heads = num_heads

    def forward(self, x):  # B, C, Height, Width
        batch_size = x.size(0)
        query = self.query(x).view(batch_size, self.ch // 8, self.num_heads, -1)  # B, C, H, T
        key = self.max_pool(self.key(x)).view(batch_size, self.ch // 8, self.num_heads, -1)  # B, C, H, T
        value = self.max_pool(self.value(x)).view(batch_size, self.ch // 2, self.num_heads, -1)  # B, C, H, T
        attention = F.softmax(
            torch.matmul(query.permute(0, 2, 3, 1), key.permute(0, 2, 1, 3)) / np.sqrt(self.ch // 8), -1)
        # attention is now B, H, T, T and Value is B, C, H, T -> B, H, T, C
        o = torch.matmul(attention,
                         value.permute(0, 2, 3, 1)).permute(0, 1, 3, 2).contiguous().view(batch_size,
                                                                                          self.ch // 2 * self.num_heads,
                                                                                          x.size(2), x.size(3))
        return self.gamma * self.o(o) + x


def main():
    assert PositionalEmbedding2d(4, 8, learned=False)(None).shape == (1, 4, 8, 8)
    assert PositionalEmbedding2d(4, 8, learned=True)(None).shape == (1, 4, 8, 8)
    x = torch.randn(3, 32, 8, 8)
    for num_heads in (1, 2):
        for use_max_pooling in (True, False):
            sa = VanillaSelfAttention2d(32, use_max_pooling, num_heads)
            assert sa(x).shape == (3, 32, 8, 8)


if __name__ == '__main__':
    main()
