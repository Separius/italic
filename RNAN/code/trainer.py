import os
import torch
import numpy as np
import torch.nn as nn
from decimal import Decimal
import torch.nn.functional as F

import utility
from model import Model
from model.cae import CAE


class Trainer:
    def __init__(self, args, loader, model, checkpoint):
        self.args = args
        self.ckp = checkpoint
        self.loader_train = loader.loader_train
        self.loader_test = loader.loader_test
        self.model = model
        patch_size = utility.get_patch_size(args.dataset)
        self.encoder = CAE(patch_size) if args.cae else None
        self.encoder = Model.place_model(self.encoder, args)
        self.optimizer = utility.make_optimizer(args, self.model, self.encoder)
        self.scheduler = utility.make_scheduler(args, self.optimizer)
        self.stages = args.stages
        self.mask_map = utility.generate_16_map(patch_size, args.cpu)
        self.best_val_loss = float('inf')  # TODO we should also load this from the saved file
        self.is_fast_test = args.test
        self.policy = args.policy
        self.loss_function = nn.MSELoss(reduction='sum')

        if self.args.resume != 0:
            Model.load_model(self.optimizer, torch.load(os.path.join(checkpoint.dir, 'optimizer.pt')), args.cpu)
            if self.encoder is not None:
                Model.load_model(self.encoder, torch.load(os.path.join(checkpoint.dir, 'model', 'encoder_latest.pt')),
                                 args.cpu)
            self.scheduler.step(self.args.resume)

        self.error_last = 1e8  # TODO we should also load this from the saved file

        self.ckp.write_log('number of parameters in the enhancer: {}'.format(utility.count_parameters(self.model)))
        if self.encoder is not None:
            self.ckp.write_log('number of parameters in the encoder: {}'.format(utility.count_parameters(self.encoder)))

    def calc_loss(self, sr, hr, mask, next_mask, stage):
        if self.policy != 'constant':
            B, _, H, W = sr.size()
            if self.policy == 'softmax_16':
                policy = F.unfold(sr[:, -1:], kernel_size=4, padding=0, stride=4)
                prev_mask = F.unfold(mask, kernel_size=4, padding=0, stride=4)
                policy = policy * (1.0 - prev_mask) + prev_mask * -1e20
                policy = F.softmax(policy, dim=1)
                next_mask = torch.zeros(B, 16, policy.size(2)).to(policy)
                next_mask.scatter_(1, policy.argmax(dim=1, keepdim=True), 1)
                next_mask.add_(prev_mask)
                next_mask = F.fold(next_mask, kernel_size=4, output_size=H, stride=4)
                policy = F.fold(policy, kernel_size=4, output_size=H, stride=4)
                effective_mask = policy
            elif self.policy == 'sigmoid_cost':
                policy = torch.sigmoid(sr[:, -1:])
                policy = policy * (1.0 - mask)
                thresholds = policy.view(B, 1, -1).topk(k=H * W // 16, dim=-1).values[:, 0, -1].view(B, 1, 1, 1).expand(
                    -1, -1, H, W)
                next_mask = (policy >= thresholds).float() + mask
                effective_mask = policy
                # stage1: policy.mean > 1/16, (policy+mask).mean > 2/16
                extra_cost = F.relu(1 / 16 - (policy + mask).mean(dim=1) / (stage + 1))
            loss = (torch.pow(sr[:-1] - hr, 2).sum(dim=1, keepdim=True) * effective_mask).sum() / effective_mask.sum()
        else:
            effective_mask = next_mask - mask
            loss = self.loss_function(sr * effective_mask, hr * effective_mask) / effective_mask.sum()
        return loss, next_mask

    def merge(self, lr, hr, stage=None):
        B, _, H, W = hr.size()
        if stage is None:
            stage = np.random.randint(self.stages, size=(B,))
            mask = torch.stack([self.mask_map[p].view(1, H, W) for p in stage], dim=0)
            next_mask = torch.stack([self.mask_map[p + 1].view(1, H, W) for p in stage], dim=0)
        else:
            mask = self.mask_map[stage].view(1, 1, H, W).expand(B, -1, -1, -1)
            next_mask = self.mask_map[stage + 1].view(1, 1, H, W).expand(B, -1, -1, -1)
        return torch.cat([lr, hr * mask, mask], dim=1), hr, mask, next_mask

    def step(self, is_train, generate_flif=None):
        if is_train:
            self.scheduler.step()
            epoch = self.scheduler.last_epoch + 1
            lr = self.scheduler.get_lr()[0]
            self.ckp.write_log('[Epoch {}]\tLearning rate: {:.2e}'.format(epoch, Decimal(lr)))
            self.model.train()
        else:
            self.model.eval()
        total_loss = 0.0
        timer_model = utility.Timer()
        torch.set_grad_enabled(is_train)
        if not is_train:
            loss_per_stage = {i: [] for i in range(self.stages)}
            image_samples = {}
        val_accumulate_batches = 2 if self.is_fast_test else 10
        for batch, (orig_lr, hr, lr_size) in enumerate(self.loader_train if is_train else self.loader_test):
            if self.is_fast_test:
                if batch > 10 and not is_train:
                    break
                if batch > 2 and is_train:
                    break
            elif batch == val_accumulate_batches and not is_train:
                break
            timer_model.tic()
            orig_lr, hr = self.prepare([orig_lr, hr])
            if self.encoder is not None:
                orig_lr, encoder_code, encoder_latent = self.encoder(hr)
            else:
                encoder_latent = None
            # TODO run for self.stages time (with backward) and divide loss by self.stages if policy != 'constant'
            for stage in range(1 if is_train else self.stages):
                # TODO change merge based on policy (next_mask is given by the net itself)
                lr, hr, mask, next_mask = self.merge(orig_lr, hr, None if is_train else stage)
                if encoder_latent is not None:
                    lr = torch.cat([lr, encoder_latent], dim=1)
                if is_train:
                    self.optimizer.zero_grad()
                sr = self.model(lr)
                # TODO calculate loss and next_mask based on sr[:, -1] and mask + divide loss by self.stages
                loss,next_mask = self.calc_loss(sr, hr, mask, next_mask, stage)
                total_loss += loss.item()
                if is_train:
                    if loss.item() < self.args.skip_threshold * self.error_last:
                        loss.backward()
                        self.optimizer.step()
                    else:
                        print('Skip this batch {}! (Loss: {})'.format(batch + 1, loss.item()))
                if not is_train:
                    loss_per_stage[stage].append(loss.item())
                    if batch < val_accumulate_batches:
                        new_image_samples = {
                            'orig': orig_lr,
                            'input': lr[:, :3],
                            'target': hr,
                            'mask': mask,
                            'output': sr,
                            'lr_size': lr_size
                        }
                        if batch == 0:
                            image_samples[stage] = new_image_samples
                        else:
                            image_samples[stage] = {k: torch.cat([v, new_image_samples[k]], dim=0) for k, v in
                                                    image_samples[stage].items()}
                    if batch == val_accumulate_batches - 1:
                        image_samples[stage] = {
                            k: v.cpu().permute(0, 2, 3, 1).numpy() if v.dim() > 2 else v.cpu().numpy() for k, v in
                            image_samples[stage].items()}
                        if stage == self.stages - 1:
                            generate_flif(image_samples)

            timer_model.hold()
            if (batch + 1) % self.args.print_every == 0:
                self.ckp.write_log('{} [{}/{}]\t{:.8f} in {:.1f}s'.format(
                    'Train' if is_train else 'Val',
                    (batch + 1) * self.args.batch_size * (1 if is_train else 2),
                    len(self.loader_train.dataset if is_train else self.loader_test.dataset),
                    total_loss / (batch + 1) / (1 if is_train else self.stages),
                    timer_model.release()))
                total_loss = 0
        if is_train:
            self.error_last = total_loss / (batch + 1)
        else:
            loss_per_stage = {k: sum(v) / len(v) for k, v in loss_per_stage.items()}
            self.ckp.write_log('DetailedLoss: {}'.format(loss_per_stage), print_as_well=False)
            val_loss = sum(loss_per_stage.values())
            is_best = val_loss < self.best_val_loss
            if is_best:
                self.best_val_loss = val_loss
            return is_best

    def prepare(self, l):
        def _prepare(tensor):
            if not self.args.cpu: tensor = tensor.cuda()
            if self.args.precision == 'half': tensor = tensor.half()
            return tensor

        return [_prepare(_l) for _l in l]

    def terminate(self):
        epoch = self.scheduler.last_epoch + 1
        return epoch >= self.args.epochs
