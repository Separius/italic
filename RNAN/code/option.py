import torch
import argparse


def parse_args(give_args=None):
    parser = argparse.ArgumentParser(description='UTLC')
    # Hardware specifications
    parser.add_argument('--cpu', action='store_true', help='use cpu only')
    parser.add_argument('--n_GPUs', type=int, default=1, help='number of GPUs')
    parser.add_argument('--seed', type=int, default=1, help='random seed')
    # Data specifications
    parser.add_argument('--batch_size', type=int, default=10)
    parser.add_argument('--stages', type=int, default=16)
    parser.add_argument('--policy', default='constant', choices=('softmax_16', 'constant', 'sigmoid_cost'))
    parser.add_argument('--dataset', default='tiny', choices=('cifar', 'imagenet32', 'tiny', 'imagenet64'))
    # Model specifications
    parser.add_argument('--n_feats', type=int, default=64, help='number of feature maps')
    parser.add_argument('--n_resgroups', type=int, default=10,
                        help='number of residual groups, 2 non local and N-2 locals')
    parser.add_argument('--rnan_prelu', action='store_true', help='use PReLU(1) in the residual blocks')
    parser.add_argument('--precision', type=str, default='single', choices=('single', 'half'))
    parser.add_argument('--factorized', action='store_true', help='use factorized attention in non local blocks')
    parser.add_argument('--cae', action='store_true', help='use cae as encoder')
    # Training specifications
    parser.add_argument('--test', action='store_true')
    parser.add_argument('--epochs', type=int, default=40, help='number of epochs to train')
    # Optimization specifications
    parser.add_argument('--lr', type=float, default=1e-4, help='learning rate')
    parser.add_argument('--lr_decay', type=int, default=25, help='learning rate decay per N epochs')
    parser.add_argument('--decay_type', type=str, default='step', help='learning rate decay type')
    parser.add_argument('--gamma', type=float, default=0.2, help='learning rate decay factor for step decay')
    parser.add_argument('--optimizer', default='ADAM', choices=('SGD', 'ADAM', 'RMSprop'))
    parser.add_argument('--momentum', type=float, default=0.9, help='SGD momentum')
    parser.add_argument('--beta1', type=float, default=0.9, help='ADAM beta1')
    parser.add_argument('--beta2', type=float, default=0.999, help='ADAM beta2')
    parser.add_argument('--epsilon', type=float, default=1e-8, help='ADAM epsilon for numerical stability')
    parser.add_argument('--weight_decay', type=float, default=0, help='weight decay')
    # Loss specifications
    parser.add_argument('--skip_threshold', type=float, default='1e6', help='skipping batch that has large error')
    # Experiment specifications
    parser.add_argument('--exp_name', type=str, default='test')
    parser.add_argument('--resume', type=int, default=0, help='epoch number to resume')
    # Log specifications
    parser.add_argument('--print_every', type=int, default=100,
                        help='how many batches to wait before logging training status')
    args = parser.parse_args(give_args)
    if args.epochs == 0:
        args.epochs = 1e8
    for arg in vars(args):
        if vars(args)[arg] == 'True':
            vars(args)[arg] = True
        elif vars(args)[arg] == 'False':
            vars(args)[arg] = False
    if not torch.cuda.is_available():
        args.cpu = True
    if args.seed > 0:
        torch.manual_seed(args.seed)
        if not args.cpu:
            torch.cuda.manual_seed(args.seed)
    return args
