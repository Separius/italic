import os
from skimage import io
from torchvision.transforms import ToTensor
from torch.utils.data import Dataset, DataLoader
import numpy as np

import torch
import torchvision
import torchvision.transforms as transforms
import scipy.ndimage
from os import listdir
from os.path import isfile, join
from tqdm import tqdm

transform = transforms.Compose(
    [transforms.ToTensor()])


def cifar_10(path,image_path):
    assert isinstance(path, str), "Expected a string input for the path"
    assert os.path.exists(path), "Input path doesn't exist"
    if not os.path.exists(image_path):
        os.makedirs(image_path)
    batch_size = 4
    trainset = torchvision.datasets.CIFAR10(root=path, train=True,download=False,transform=transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size,shuffle=False, num_workers=2)
    testset = torchvision.datasets.CIFAR10(root=path, train=False,download=False)
    testloader = torch.utils.data.DataLoader(testset, batch_size=4,shuffle=False, num_workers=2)
    cnt = 0
    for i, (data,_) in enumerate(trainloader):
        data = data.permute(0,2,3,1).numpy()
        for j in range(batch_size):
            io.imsave(image_path+str(cnt)+'.png',data[j])
            cnt += 1
