import os
from PIL import Image
from glob import glob
from torchvision.transforms import ToTensor
from torch.utils.data import Dataset, DataLoader
import pdb
from utility import get_size_of_file


class TinyImageNet(Dataset):
    def __init__(self, path, is_train=True):
        super().__init__()
        self.image_addresses = glob(
            os.path.expanduser(os.path.join(path, 'train/*/images/*.JPEG' if is_train else 'val/images/*.JPEG')),
            recursive=True)
        self.lr_image_addresses = [v.replace('.JPEG', '_bpg.png') for v in self.image_addresses]
        self.encoded_image_addresses = [v.replace('.JPEG', '.bpg') for v in self.image_addresses]
        self.to_tensor = ToTensor()

    def __len__(self):
        return len(self.image_addresses)

    def __getitem__(self, index):
        image1, image2 = Image.open(self.lr_image_addresses[index]), Image.open(self.image_addresses[index])
        return self.to_tensor(image1.convert("RGB")), self.to_tensor(image2.convert("RGB")), get_size_of_file(
            self.encoded_image_addresses[index])


class TinyLoader:
    def __init__(self, batch_size=10, path='~/Downloads/tiny-imagenet-200'):
        self.dataset_train = TinyImageNet(path, is_train=True)
        self.dataset_val = TinyImageNet(path, is_train=False)
        self.loader_train = DataLoader(self.dataset_train, batch_size=batch_size,
                                       shuffle=True, drop_last=True, num_workers=4)
        self.loader_test = DataLoader(self.dataset_val, batch_size=batch_size * 2,
                                      shuffle=True, drop_last=False, num_workers=4)


class Imagenet_64(Dataset):
    def __init__(self, path, is_train=True):
        super().__init__()
        self.image_addresses = glob(
            os.path.expanduser(os.path.join(path, 'train/*.png' if is_train else 'valid/*.png')),
            recursive=True)
        self.lr_image_addresses = [v.replace('.png', '_bpg.png') for v in self.image_addresses]
        self.encoded_image_addresses = [v.replace('.png', '.bpg') for v in self.image_addresses]
        self.to_tensor = ToTensor()

    def __len__(self):
        return len(self.image_addresses)

    def __getitem__(self, index):
        image1, image2 = Image.open(self.lr_image_addresses[index]), Image.open(self.image_addresses[index])
        return self.to_tensor(image1.convert("RGB")), self.to_tensor(image2.convert("RGB")), get_size_of_file(
            self.encoded_image_addresses[index])


class Imagenet64Loader:
    def __init__(self, batch_size=10, path='/hdd/Danial/Compression_Dataset/Danial_download/ImageNet_64'):
        self.dataset_train = TinyImageNet(path, is_train=True)
        self.dataset_val = TinyImageNet(path, is_train=False)
        self.loader_train = DataLoader(self.dataset_train, batch_size=batch_size,
                                       shuffle=True, drop_last=True, num_workers=4)
        self.loader_test = DataLoader(self.dataset_val, batch_size=batch_size * 2,
                                      shuffle=True, drop_last=False, num_workers=4)


class Imagenet_32(Dataset):
    def __init__(self, path, is_train=True):
        super().__init__()
        self.image_addresses = glob(
            os.path.expanduser(os.path.join(path, 'train/*.png' if is_train else 'valid/*.png')),
            recursive=True)
        self.lr_image_addresses = [v.replace('.png', '_bpg.png') for v in self.image_addresses]
        self.encoded_image_addresses = [v.replace('.png', '.bpg') for v in self.image_addresses]
        self.to_tensor = ToTensor()

    def __len__(self):
        return len(self.image_addresses)

    def __getitem__(self, index):
        image1, image2 = Image.open(self.lr_image_addresses[index]), Image.open(self.image_addresses[index])
        return self.to_tensor(image1.convert("RGB")), self.to_tensor(image2.convert("RGB")), get_size_of_file(
            self.encoded_image_addresses[index])


class Imagenet32Loader:
    def __init__(self, batch_size=10, path='/hdd/Danial/Compression_Dataset/Danial_download/ImageNet_32'):
        self.dataset_train = TinyImageNet(path, is_train=True)
        self.dataset_val = TinyImageNet(path, is_train=False)
        self.loader_train = DataLoader(self.dataset_train, batch_size=batch_size,
                                       shuffle=True, drop_last=True, num_workers=4)
        self.loader_test = DataLoader(self.dataset_val, batch_size=batch_size * 2,
                                      shuffle=True, drop_last=False, num_workers=4)




