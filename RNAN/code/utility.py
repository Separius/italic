import os
import time
import torch
import datetime
import subprocess
import numpy as np
import imageio as io
import torch.optim as optim
import torch.optim.lr_scheduler as lrs


class Timer:
    def __init__(self):
        self.acc = 0
        self.tic()

    def tic(self):
        self.t0 = time.time()

    def toc(self):
        return time.time() - self.t0

    def hold(self):
        self.acc += self.toc()

    def release(self):
        ret = self.acc
        self.acc = 0
        return ret

    def reset(self):
        self.acc = 0


class Checkpoint:
    def __init__(self, args):
        self.args = args
        now = datetime.datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
        self.encoding_bpp = -1 if args.cae else 0  # TODO Calculate bpp for CAE
        self.dir = '../experiment/' + args.exp_name

        if os.path.exists(self.dir) and args.resume == 0:
            print('Warning, change resume to continue training')
            user_input = input('should we continue?(yes/no)')
            if not user_input.startswith('y'):
                exit(0)
        if not os.path.exists(self.dir):
            args.resume = 0

        def _make_dir(path):
            if not os.path.exists(path):
                os.makedirs(path)

        _make_dir(self.dir)
        _make_dir(self.dir + '/model')
        _make_dir(self.dir + '/results')

        open_type = 'a' if os.path.exists(self.dir + '/log.txt') else 'w'
        self.log_file = open(self.dir + '/log.txt', open_type)
        with open(self.dir + '/config.txt', open_type) as f:
            f.write(now + '\n\n')
            for arg in vars(args):
                f.write('{}: {}\n'.format(arg, getattr(args, arg)))
            f.write('\ngit-commit: ' + str(subprocess.check_output(['git', 'rev-parse', 'HEAD'])) + '\n')

    def save(self, trainer, is_best=False):
        trainer.model.save(self.dir, is_best=is_best)

        if trainer.encoder is not None:
            torch.save(
                trainer.encoder.state_dict(),
                os.path.join(self.dir, 'model', 'encoder_latest.pt')
            )
            if is_best:
                torch.save(
                    trainer.encoder.state_dict(),
                    os.path.join(self.dir, 'model', 'encoder_best.pt')
                )
        torch.save(
            trainer.optimizer.state_dict(),
            os.path.join(self.dir, 'optimizer.pt')
        )

    def write_log(self, log, refresh=False, print_as_well=True):
        if print_as_well:
            print(log)
        self.log_file.write(log + '\n')
        if refresh:
            self.log_file.close()
            self.log_file = open(self.dir + '/log.txt', 'a')

    def done(self):
        self.log_file.close()

    def generate_flif_input(self, image_samples, num_stages, epoch, patch_size):
        output = 0
        for stage in range(num_stages):
            if stage != num_stages - 1:
                output += image_samples[stage]['output'] * (
                        image_samples[stage + 1]['mask'] - image_samples[stage]['mask'])
            else:
                output += image_samples[stage]['output'] * (
                        np.ones_like(image_samples[stage]['mask']) - image_samples[stage]['mask'])
        output = int_quantize(output)
        target = int_quantize(image_samples[0]['target'])
        batch_size = image_samples[0]['orig'].shape[0]
        flif_size = []
        lr_size = image_samples[0]['lr_size']
        our_method_size = []
        file_names_template = dict(
            model_output='{}/{}_{}_output.png',
            target='{}/{}_{}_target.png',
            model_flif='{}/{}_{}.flif',
            orig_flif='{}/{}_{}_orig.flif',
        )
        for b in range(batch_size):
            file_names = {k: v.format(self.dir, epoch, b) for k, v in file_names_template.items()}
            io.imsave(file_names['model_output'], output[b])
            io.imsave(file_names['target'], target[b])
            os.system('../../flif/src/flif --overwrite -e -n {} {} {}'.format(
                file_names['target'], file_names['model_output'], file_names['model_flif']))
            os.system('../../flif_orig/flif -e -n {} {}'.format(
                file_names['target'], file_names['orig_flif']))
            flif_size.append(get_size_of_file(file_names['orig_flif']))
            our_method_size.append(get_size_of_file(file_names['model_flif']))
            for file_name in file_names.values():
                os.system('rm {}'.format(file_name))
        scale_factor = patch_size * patch_size / 8
        ours = np.array(our_method_size).mean() / scale_factor
        if self.encoding_bpp != 0:
            ours += self.encoding_bpp
        else:
            ours += np.array(lr_size).mean() / scale_factor
        self.write_log('epoch:{} -> mean_flif(bpp): {}\t mean_ours(bpp): {}'
                       .format(epoch, np.array(flif_size).mean() / scale_factor, ours))


def trainable_parameters(model):
    return filter(lambda x: x.requires_grad, model.parameters())


def make_optimizer(args, my_model, encoder=None):
    trainable = trainable_parameters(my_model)
    if args.optimizer == 'SGD':
        optimizer_function = optim.SGD
        kwargs = {'momentum': args.momentum}
    elif args.optimizer == 'ADAM':
        optimizer_function = optim.Adam
        kwargs = {
            'betas': (args.beta1, args.beta2),
            'eps': args.epsilon
        }
    elif args.optimizer == 'RMSprop':
        optimizer_function = optim.RMSprop
        kwargs = {'eps': args.epsilon}
    else:
        raise ValueError('invalid optimizer')

    kwargs['lr'] = args.lr
    kwargs['weight_decay'] = args.weight_decay
    if encoder is None:
        return optimizer_function(trainable, **kwargs)
    return optimizer_function([{'params': trainable},
                               {'params': trainable_parameters(encoder), 'lr': 3e-4, 'weight_decay': 0.0}], **kwargs)


def make_scheduler(args, optimizer):
    if args.decay_type == 'step':
        scheduler = lrs.StepLR(
            optimizer,
            step_size=args.lr_decay,
            gamma=args.gamma
        )
    elif args.decay_type.find('step') >= 0:
        milestones = args.decay_type.split('_')
        milestones.pop(0)
        milestones = list(map(lambda x: int(x), milestones))
        scheduler = lrs.MultiStepLR(
            optimizer,
            milestones=milestones,
            gamma=args.gamma
        )
    else:
        raise ValueError('invalid scheduler')
    return scheduler


def count_parameters(model):
    return sum(p.numel() for p in trainable_parameters(model))


def generate_16_map(size=64, cpu=False):
    assert size % 16 == 0
    mask_map = {
        0: np.array([[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]),
        1: np.array([[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]),
        2: np.array([[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 1, 0], [0, 0, 0, 0]]),
        3: np.array([[1, 0, 1, 0], [0, 0, 0, 0], [0, 0, 1, 0], [0, 0, 0, 0]]),
        4: np.array([[1, 0, 1, 0], [0, 0, 0, 0], [1, 0, 1, 0], [0, 0, 0, 0]]),
        5: np.array([[1, 0, 1, 0], [0, 1, 0, 0], [1, 0, 1, 0], [0, 0, 0, 0]]),
        6: np.array([[1, 0, 1, 0], [0, 1, 0, 1], [1, 0, 1, 0], [0, 0, 0, 0]]),
        7: np.array([[1, 0, 1, 0], [0, 1, 0, 1], [1, 0, 1, 0], [0, 1, 0, 0]]),
        8: np.array([[1, 0, 1, 0], [0, 1, 0, 1], [1, 0, 1, 0], [0, 1, 0, 1]]),
        9: np.array([[1, 1, 1, 0], [0, 1, 0, 1], [1, 0, 1, 0], [0, 1, 0, 1]]),
        10: np.array([[1, 1, 1, 1], [0, 1, 0, 1], [1, 0, 1, 0], [0, 1, 0, 1]]),
        11: np.array([[1, 1, 1, 1], [0, 1, 0, 1], [1, 1, 1, 0], [0, 1, 0, 1]]),
        12: np.array([[1, 1, 1, 1], [0, 1, 0, 1], [1, 1, 1, 1], [0, 1, 0, 1]]),
        13: np.array([[1, 1, 1, 1], [1, 1, 0, 1], [1, 1, 1, 1], [0, 1, 0, 1]]),
        14: np.array([[1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1], [0, 1, 0, 1]]),
        15: np.array([[1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 0, 1]]),
        16: np.array([[1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1]]),
    }
    mask_map = {k: torch.from_numpy(np.tile(v, (size // 4, size // 4)).astype(np.float32))
                for k, v in mask_map.items()}
    if not cpu:
        mask_map = {k: v.cuda() for k, v in mask_map.items()}
    return mask_map


def get_size_of_file(path):
    return os.stat(path).st_size


def int_quantize(image):
    return np.clip((image * 255 + 0.5), 0, 255).astype(np.uint8)


def get_patch_size(dataset):
    if dataset in ('cifar', 'imagenet32'):
        return 32
    if dataset in ('tiny', 'imagenet64'):
        return 64
    raise ValueError('invalid dataset name')
