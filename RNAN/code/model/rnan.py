import torch.nn as nn

from model.common import default_conv, ResAttModuleDownUpPlus


### (nonlocal)residual attention + downscale upscale + denoising
class _ResGroup(nn.Sequential):
    def __init__(self, conv, n_feats, kernel_size, act, is_nonlocal=False, is_factorized=False):
        super().__init__(ResAttModuleDownUpPlus(conv, n_feats, kernel_size, act, is_nonlocal, is_factorized),
                         conv(n_feats, n_feats, kernel_size))


class RNAN(nn.Module):
    def __init__(self, args, conv=default_conv):
        super().__init__()
        n_resgroup = args.n_resgroups
        n_feats = args.n_feats
        kernel_size = 3
        uses_trainable_encoder = args.cae
        act = nn.PReLU() if args.rnan_prelu else nn.ReLU(True)
        self.colors = 3
        # define head module
        if uses_trainable_encoder:
            extra_channels = 32
        else:
            extra_channels = 0
        # masked_hr + lr + mask + extra_channels
        self.head = conv(self.colors * 2 + 1 + extra_channels, n_feats, kernel_size)
        # define body module
        self.body_nl_low = _ResGroup(conv, n_feats, kernel_size, act, is_nonlocal=True, is_factorized=args.factorized)
        self.body = nn.Sequential(*[_ResGroup(conv, n_feats, kernel_size, act) for _ in range(n_resgroup - 2)],
                                  conv(n_feats, n_feats, kernel_size))
        self.body_nl_high = _ResGroup(conv, n_feats, kernel_size, act, is_nonlocal=True, is_factorized=args.factorized)
        # define tail module
        self.tail = conv(n_feats, self.colors + (0 if args.policy == 'constant' else 1), kernel_size)

    def forward(self, x):
        feats_shallow = self.head(x)
        res = self.body_nl_low(feats_shallow)
        res = self.body(res)
        res = self.body_nl_high(res)
        res_main = self.tail(res)
        res_main[:, :self.colors] = res_main[:, :self.colors] + x[:, :self.colors]
        return res_main
