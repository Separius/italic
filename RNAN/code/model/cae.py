import torch
import numpy as np
from torch import nn

from coding.huffman import huffman_encode


def compute_psnr(x, y):
    y = y.view(y.shape[0], -1)
    x = x.view(x.shape[0], -1)
    rmse = torch.rsqrt(torch.mean((y - x) ** 2, dim=1))
    psnr = torch.mean(20. * torch.log10(rmse))
    return psnr


def conv_downsample(in_planes, out_planes):
    return nn.Sequential(
        nn.ReflectionPad2d(2),
        nn.Conv2d(in_planes, out_planes, kernel_size=5, stride=2, bias=False),
        nn.BatchNorm2d(out_planes),
    )


def conv_same(in_planes, out_planes):
    return nn.Sequential(
        nn.ReflectionPad2d(1),
        nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=1, bias=False),
        nn.BatchNorm2d(out_planes),
    )


def sub_pix(in_planes, out_planes, upscale_factor):
    mid_planes = upscale_factor ** 2 * out_planes
    return nn.Sequential(
        nn.Conv2d(in_planes, mid_planes, kernel_size=1, stride=1, bias=False),
        nn.BatchNorm2d(mid_planes),
        nn.PixelShuffle(upscale_factor)
    )


def quantize(x):
    with torch.no_grad():
        floor = x.floor()
        r = torch.rand(x.shape).cuda()
        p = x - floor
        eps = torch.zeros(x.shape).cuda()
        eps[r <= p] = (floor + 1 - x)[r <= p]
        eps[r > p] = (floor - x)[r > p]
    y = x + eps
    return y


# class Bottleneck(nn.Module):
#     def __init__(self, planes):
#         super().__init__()
#         self.act = nn.PReLU()
#         self.net = nn.Sequential(
#             nn.Conv2d(planes, planes, kernel_size=1, bias=False),
#             nn.BatchNorm2d(planes), self.act,
#             nn.Conv2d(planes, planes, kernel_size=3, stride=1, padding=1, bias=False),
#             nn.BatchNorm2d(planes), self.act,
#             nn.Conv2d(planes, planes, kernel_size=1, bias=False),
#             nn.BatchNorm2d(planes)
#         )

#     def forward(self, x):
#         return self.act(self.net(x) + x)



class Bottleneck(nn.Module):
    def __init__(self, planes):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(planes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, planes, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(planes)
        self.relu1 = nn.PReLU()
        self.relu2 = nn.PReLU()
        self.relu3 = nn.PReLU()

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu1(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu2(out)

        out = self.conv3(out)
        out = self.bn3(out)

        out += residual
        out = self.relu3(out)

        return out



def res_layers(planes, num_blocks, ):
    return nn.Sequential(*[Bottleneck(planes) for _ in range(num_blocks)])


def compute_bpp(code, batch_size, prefix, dir='./code/', save=False):
    # Huffman coding
    c = code.data.cpu().numpy().astype(np.int32).flatten()
    tree_size, data_size = huffman_encode(c, prefix, save_dir=dir, save=save)
    bpp = (tree_size + data_size) / batch_size / 128 / 128 * 8
    return bpp


class CAE(nn.Module):
    # how to train https://github.com/JasonZHM/CAE-ADMM/blob/master/CAE_ADMM/train.py
    def __init__(self, patch_size=64, num_resblocks=15, final_len=32):
        super().__init__()
        self.num_resblocks = num_resblocks
        self.threshold = torch.Tensor([1e-4])
        self.prune = False

        # Encoder
        self.E_Conv_1 = conv_same(3, 32)  # 3,patch_size,patch_size => 32,ps,ps
        self.E_PReLU_1 = nn.PReLU()
        self.E_Conv_2 = conv_downsample(32, 64)  # 32,128,128 => 64,ps/2,ps/2
        self.E_PReLU_2 = nn.PReLU()
        self.E_Conv_3 = conv_same(64, 128)  # 64,64,64 => 128,ps/2,ps/2
        self.E_PReLU_3 = nn.PReLU()
        self.E_Res = res_layers(128, num_blocks=self.num_resblocks)
        # TODO add nonlocal here or after E_Conv_4(maybe based on patch_size)
        self.E_Conv_4 = conv_downsample(128, 64)  # 128,ps/2,ps/2 => 64,ps/4,ps/4
        self.E_Conv_5 = conv_same(64, 32)  # 64,ps/4,ps/4 => 32,ps/4,ps/4
        self.E_Conv_6 = conv_same(32, final_len)

        self.Pruner = nn.Threshold(self.threshold, 0, inplace=True)

        # Decoder
        self.D_SubPix_00 = sub_pix(final_len, 32, 1)  # 32,ps/4,ps/4 => 32,ps/4,ps/4
        self.D_SubPix_0 = sub_pix(32, 64, 1)  # for fine tuning (64, ps/4, ps/4)
        # TODO add nonlocal here or after D_SubPix_1(maybe based on patch_size)
        self.D_SubPix_1 = sub_pix(64, 128, 2)  # 64,ps/4,ps/4 => 128,ps/2,ps/2
        self.D_PReLU_1 = nn.PReLU()
        self.D_Res = res_layers(128, num_blocks=self.num_resblocks)
        self.D_SubPix_2 = sub_pix(128, 64, 1)  # 128,ps/2,ps/2 => 64,ps/2,ps/2
        self.D_PReLU_2 = nn.PReLU()
        self.D_SubPix_3 = sub_pix(64, 32, 2)  # 64,ps/2,ps/2 => 32,ps,ps (NOTE: this is the latent)
        self.D_PReLU_3 = nn.PReLU()
        self.D_SubPix_4 = sub_pix(32, 3, 1)  # 32,ps,ps => 3,ps,ps
        self.tanh = nn.Tanh()

        self.__init_parameters__()

    def __init_parameters__(self):
        # Initialize Parameters
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out')
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
            if isinstance(m, Bottleneck):
                nn.init.kaiming_normal_(m.conv1.weight, mode='fan_out')
                nn.init.kaiming_normal_(m.conv2.weight, mode='fan_out')
                nn.init.kaiming_normal_(m.conv3.weight, mode='fan_out')
                nn.init.constant_(m.bn3.weight, 0)

    def prune_mode(self, prune=True, threshold=1e-4):
        self.prune = prune
        self.threshold = torch.Tensor([threshold])
        self.Pruner.threshold = self.threshold

    def forward(self, x):
        x = self.E_Conv_1(x)
        x = self.E_PReLU_1(x)
        x = self.E_Conv_2(x)
        x = self.E_PReLU_2(x)
        x = self.E_Conv_3(x)
        x = self.E_PReLU_3(x)
        x = self.E_Res(x)
        x = self.E_Conv_4(x)
        x = self.E_Conv_5(x)
        x = self.E_Conv_6(x)
        if self.prune:
            x = self.Pruner(x, self.threshold)
        code = quantize(x)
        lr = self.D_SubPix_00(code)
        lr = self.D_SubPix_0(lr)
        lr = self.D_SubPix_1(lr)
        lr = self.D_PReLU_1(lr)
        lr = self.D_Res(lr)
        lr = self.D_SubPix_2(lr)
        lr = self.D_PReLU_2(lr)
        lr = self.D_SubPix_3(lr)
        lr = self.D_PReLU_3(lr)  # this is latent with size of x
        latent = lr
        lr = self.D_SubPix_4(lr)
        lr = (self.tanh(lr) + 1) / 2
        return lr, x, latent
