import torch
import torch.nn as nn
import torch.nn.functional as F


def default_conv(in_channels, out_channels, kernel_size, bias=True):
    return nn.Conv2d(in_channels, out_channels, kernel_size, padding=(kernel_size // 2), bias=bias)


class ResBlock(nn.Module):
    def __init__(self, conv, n_feat, kernel_size, bias=True, bn=False, act=nn.ReLU(True)):
        super().__init__()
        m = []
        for i in range(2):
            m.append(conv(n_feat, n_feat, kernel_size, bias=bias))
            if bn: m.append(nn.BatchNorm2d(n_feat))
            if i == 0: m.append(act)
        self.body = nn.Sequential(*m)

    def forward(self, x):
        return self.body(x) + x


# add NonLocalBlock2D
# reference: https://github.com/AlexHex7/Non-local_pytorch/blob/master/lib/non_local_simple_version.py
class NonLocalBlock2D(nn.Module):
    def __init__(self, in_channels, inter_channels, is_factorized=False):
        super().__init__()
        self.in_channels = in_channels
        self.inter_channels = inter_channels
        self.value = nn.Conv2d(in_channels=self.in_channels, out_channels=self.inter_channels,
                               kernel_size=1, stride=1, padding=0)

        self.W = nn.Conv2d(in_channels=self.inter_channels, out_channels=self.in_channels,
                           kernel_size=1, stride=1, padding=0)
        nn.init.constant_(self.W.weight, 0)
        nn.init.constant_(self.W.bias, 0)

        self.query = nn.Conv2d(in_channels=self.in_channels, out_channels=self.inter_channels,
                               kernel_size=1, stride=1, padding=0)

        self.key = nn.Conv2d(in_channels=self.in_channels, out_channels=self.inter_channels,
                             kernel_size=1, stride=1, padding=0)
        self.is_factorized = is_factorized

    def forward(self, x):
        batch_size = x.size(0)
        value = self.value(x).view(batch_size, self.inter_channels, -1).permute(0, 2, 1)
        query = self.query(x).view(batch_size, self.inter_channels, -1).permute(0, 2, 1)
        key = self.key(x).view(batch_size, self.inter_channels, -1)
        if self.is_factorized:
            channel_attention = torch.matmul(key, value) / key.size(-1)
            y = torch.matmul(query, channel_attention)
        else:
            spatial_attention = F.softmax(torch.matmul(query, key), dim=1)
            y = torch.matmul(spatial_attention, value)
        y = y.permute(0, 2, 1).contiguous()
        y = y.view(batch_size, self.inter_channels, *x.size()[2:])
        return self.W(y) + x


## define (nonlocal)mask branch
class MaskBranchDownUp(nn.Module):
    def __init__(self, conv, n_feat, kernel_size, act, is_nonlocal=False, is_factorized=False):
        super().__init__()
        if is_nonlocal:
            rb1 = [NonLocalBlock2D(n_feat, n_feat, is_factorized=is_factorized)]
        else:
            rb1 = []
        rb1.append(ResBlock(conv, n_feat, kernel_size, act=act))
        self.head = nn.Sequential(*rb1)
        self.middle = nn.Sequential(
            nn.Conv2d(n_feat, n_feat, 3, stride=2, padding=1),
            *[ResBlock(conv, n_feat, kernel_size, act=act) for _ in range(2)],
            nn.ConvTranspose2d(n_feat, n_feat, 6, stride=2, padding=2)
        )
        self.tail = nn.Sequential(
            ResBlock(conv, n_feat, kernel_size, act=act),
            nn.Conv2d(n_feat, n_feat, 1, padding=0, bias=True),
            nn.Sigmoid()
        )

    def forward(self, x):
        h = self.head(x)
        return self.tail(h + self.middle(h))


## define (nonlocal)residual attention module
class ResAttModuleDownUpPlus(nn.Module):
    def __init__(self, conv, n_feat, kernel_size, act, is_nonlocal=False, is_factorized=False):
        super().__init__()
        self.RA_RB1 = ResBlock(conv, n_feat, kernel_size, act=act)  # TODO I think should be repeated 2 times(q=2)
        self.RA_TB = nn.Sequential(*[ResBlock(conv, n_feat, kernel_size, act=act) for _ in range(2)])  # t=2
        self.RA_MB = MaskBranchDownUp(conv, n_feat, kernel_size, act, is_nonlocal, is_factorized)
        self.RA_tail = nn.Sequential(*[ResBlock(conv, n_feat, kernel_size, act=act) for _ in range(2)])  # q=2

    def forward(self, input):
        RA_RB1_x = self.RA_RB1(input)
        tx = self.RA_TB(RA_RB1_x)
        mx = self.RA_MB(RA_RB1_x)
        return self.RA_tail(tx * mx + RA_RB1_x)
