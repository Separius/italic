import os
import torch
import torch.nn as nn

from model.rnan import RNAN


class Model(nn.Module):
    @staticmethod
    def place_model(model, args):
        if model is None:
            return model
        if not args.cpu:
            model = model.cuda()
            if args.precision == 'half':
                model = model.half()
            if args.n_GPUs > 1:
                gpu_list = range(0, args.n_GPUs)
                model = nn.DataParallel(model, gpu_list)
        return model

    def __init__(self, args, ckp):
        super().__init__()
        print('Making model...')
        self.precision = args.precision
        self.cpu = args.cpu
        self.n_GPUs = args.n_GPUs
        self.model = RNAN(args)
        self.model = self.place_model(self.model, args)
        if args.resume > 0:
            self.load_model(self.get_model(), os.path.join(ckp.dir, 'model', 'model_latest.pt'), args.cpu)

    def forward(self, x):
        return self.model(x)

    def get_model(self):
        if self.n_GPUs == 1:
            return self.model
        else:
            return self.model.module

    def state_dict(self, **kwargs):
        target = self.get_model()
        return target.state_dict(**kwargs)

    def save(self, apath, is_best=False):
        target = self.get_model()
        torch.save(
            target.state_dict(),
            os.path.join(apath, 'model', 'model_latest.pt')
        )
        if is_best:
            torch.save(
                target.state_dict(),
                os.path.join(apath, 'model', 'model_best.pt')
            )

    @staticmethod
    def load_model(model, path, cpu):
        kwargs = {'map_location': lambda storage, loc: storage} if cpu else {}
        model.load_state_dict(torch.load(path, **kwargs), strict=False)