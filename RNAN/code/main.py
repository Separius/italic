import torch

import utility
from model import Model
from trainer import Trainer
from option import parse_args
from dataset import TinyLoader, Imagenet32Loader, Imagenet64Loader


def main():
    args = parse_args()
    checkpoint = utility.Checkpoint(args)
    if torch.cuda.is_available():
        torch.backends.cudnn.benchmark = True
    if args.dataset == 'tiny':
        loader = TinyLoader(batch_size=args.batch_size)
    elif args.dataset == 'cifar':
        loader = None  # TODO add cifar10 (how to calculate bpg_encoded_size?)
    elif args.dataset == 'imagenet32':
        loader = Imagenet32Loader(batch_size=args.batch_size) # TODO (how to calculate bpg_encoded_size?)
    elif args.dataste == 'imagenet64':
        loader = Imagenet64Loader(batch_size=args.batch_size) # TODO(how to calculate bpg_encoded_size?)
    model = Model(args, checkpoint)
    t = Trainer(args, loader, model, checkpoint)

    def wrapped_generate_flif_input(image_samples):
        checkpoint.generate_flif_input(image_samples, args.stages, t.scheduler.last_epoch, utility.get_patch_size(args.dataset))

    try:
        while not t.terminate():
            t.step(is_train=True)
            is_best = t.step(is_train=False, generate_flif=wrapped_generate_flif_input)
            checkpoint.save(t, is_best)
    except KeyboardInterrupt:
        pass

    checkpoint.done()


if __name__ == '__main__':
    main()
