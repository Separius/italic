import cv2
import matplotlib.pyplot as plt
import numpy as np
import pdb
def huffman_size(hist):
    # pdb.set_trace()
    symbols = hist[1]
    freq = hist[0]
    inf=100000000
    total=0
    for i in range(0,len(symbols)-1):
    	m=np.min(freq)
    	mix = np.argmin(freq)
    	freq[mix] =inf
    	m2=np.min(freq)
    	mix2 = np.argmin(freq)
    	s = m+m2;
    	#	print(m,m2,s)
    	freq[mix2]=s
    	total  = total+s
    return total/8/1024
def rgb2ycocg(img):
	ycocg = np.zeros_like(img,dtype=np.int32)
	R = np.int16(img[:,:,0])
	G = np.int16(img[:,:,1])
	B = np.int16(img[:,:,2])
	Y = (((R + B)>>1) + G)>>1
	Co = R - B
	Cg = G - ((R + B)>>1)
	ycocg[:,:,0] = np.int32(Y)
	ycocg[:,:,1] = np.int32(Co)
	ycocg[:,:,2] = np.int32(Cg)
	# print(ycocg[:,:,2].min())
	return ycocg
# pdb.set_trace()
output = cv2.imread('./gen_mse_complete/pic000_epoch_0.png')
output = rgb2ycocg(output)
orig = cv2.imread('./orig/00.png')
orig = rgb2ycocg(orig)
res = orig - output
# pdb.set_trace()
size = 0
list = [511 , 1021, 1021]
for c in range(3):
    hist = np.histogram(res[:,:,c].flatten() , bins = list[c])
    size += huffman_size(hist)
print('size:' , size)
